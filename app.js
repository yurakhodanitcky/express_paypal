const createError = require("http-errors");
const express = require("express");
const path = require("path");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const config = require("config");
const swaggerUi = require("swagger-ui-express");
const swaggerJsdoc = require("swagger-jsdoc");

const indexRouter = require("./routes/index");
const usersRouter = require("./routes/users");
const paypalRouter = require("./routes/paypal");
const stripeRouter = require("./routes/stripe");
const braintreeRouter = require("./routes/braintree");

const app = express();

app.use(express.static(path.join(__dirname, "public")));

const swaggerOptions = {
  swaggerDefinition: {
    openapi: "3.0.0",
    info: {
      title: "Care API documentation",
      version: "1.0.0",
      description: "Expressjs RESTful API for care",
    },
    basePath: "/",
  },
  apis: ["./routes/paypal.js", "./routes/stripe.js", "./routes/braintree.js"],
};

const swaggerDocs = swaggerJsdoc(swaggerOptions);

app.use("/swagger", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
app.get("/swagger.json", (req, res) => {
  res.json(swaggerDocs);
});

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "hbs");

app.use(logger("dev"));
app.use(
  express.json({
    // We need the raw body to verify webhook signatures.
    // Let's compute it only when hitting the Stripe webhook endpoint.
    verify: function (req, res, buf) {
      if (req.originalUrl.startsWith("/webhook")) {
        req.rawBody = buf.toString();
      }
    },
  })
);
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(cookieParser());

app.use("/", indexRouter);
app.use("/users", usersRouter);
app.use("/paypal", paypalRouter);
app.use("/stripe", stripeRouter);
app.use("/braintree", braintreeRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
