const express = require("express");
const router = express.Router();
const request = require("request");
const {
  urlencoded
} = require("express");
const config = require("config");

const baseUrl = config.get("base_url");
const paypalAuthUrl = config.get("paypal.AUTH_URL");
const url = config.get("paypal.API_URL");
const user = config.get("paypal.USER");
const pwd = config.get("paypal.PWD");
const signature = config.get("paypal.SIGNATURE");
const version = config.get("paypal.VERSION");

/**
 * @swagger
 * tags:
 *   name: PAYPAL
 *   description: PAYPAL management
 */

/**
 * @swagger
 *
 * /paypal/auth:
 *   get:
 *     description: Auth PayPal
 *     tags: [PAYPAL]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.get("/auth", function (req, res, next) {
  var options = {
    method: "POST",
    url: url,
    headers: {
      "content-type": "application/x-www-form-urlencoded"
    },
    form: {
      USER: user,
      PWD: pwd,
      SIGNATURE: signature,
      VERSION: version,
      METHOD: "SetExpressCheckout",
      PAYMENTREQUEST_0_PAYMENTACTION: "AUTHORIZATION",
      PAYMENTREQUEST_0_AMT: 0,
      PAYMENTREQUEST_0_CURRENCYCODE: "USD",
      L_BILLINGTYPE0: "MerchantInitiatedBillingSingleAgreement",
      L_BILLINGAGREEMENTDESCRIPTION0: "ClubUsage",
      cancelUrl: baseUrl + "/paypal/cancel",
      returnUrl: baseUrl + "/paypal/success",
    },
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    let arr = decodeURIComponent(body).split("&");
    var jsonData = toJson(body);
    res.redirect(paypalAuthUrl + jsonData.TOKEN);
  });
});

/**
 * @swagger
 *
 * /paypal/success:
 *   get:
 *     description: success
 *     parameters:
 *       - in: query
 *         name: token
 *         required: true
 *         schema:
 *           type: string
 *         description: token
 *     tags: [PAYPAL]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.get("/success", function (req, res, next) {
  let token = req.query.token;
  var options = {
    method: "POST",
    url: url,
    headers: {
      "content-type": "application/x-www-form-urlencoded"
    },
    form: {
      USER: user,
      PWD: pwd,
      SIGNATURE: signature,
      VERSION: version,
      METHOD: "CreateBillingAgreement",
      TOKEN: token,
    },
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    res.json(toJson(body));
  });
});

/**
 * @swagger
 *
 * /paypal/pay:
 *   post:
 *     description: Pay
 *     tags: [PAYPAL]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.post("/pay", function (req, res, next) {
  var options = {
    method: "POST",
    url: url,
    headers: {
      "content-type": "application/x-www-form-urlencoded"
    },
    form: {
      USER: user,
      PWD: pwd,
      SIGNATURE: signature,
      VERSION: version,
      METHOD: "DoReferenceTransaction",
      PAYMENTACTION: "Sale",
      AMT: 25.0,
      CURRENCYCODE: "USD",
      REFERENCEID: "B-9NW62448FR4937613",
    },
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    res.json(toJson(body));
  });
});

/**
 * @swagger
 *
 * /paypal/pay-now:
 *   post:
 *     description: Pay
 *     tags: [PAYPAL]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.post("/pay-now", function (req, res, next) {
  var options = {
    method: "POST",
    url: url,
    headers: {
      "content-type": "application/x-www-form-urlencoded"
    },
    form: {
      USER: user,
      PWD: pwd,
      SIGNATURE: signature,
      VERSION: version,
      METHOD: "DoExpressCheckoutPayment",
      TOKEN: "EC-1M188489WV832950X",
      PAYMENTREQUEST_0_PAYMENTACTION: "SALE",
      PAYERID: "RU32KQSLA76J2",
      PAYMENTREQUEST_0_AMT: 25.0,
      PAYMENTREQUEST_0_CURRENCYCODE: "EUR",
    },
  };

  request(options, function (error, response, body) {
    if (error) throw new Error(error);
    res.json(toJson(body));
  });
});

function toJson(body) {
  let arr = decodeURIComponent(body).split("&");
  var jsonData = {};
  arr.forEach((e) => {
    let jArr = e.split("=");
    jsonData[jArr[0]] = jArr[1];
  });
  console.log(jsonData);
  return jsonData;
}

module.exports = router;