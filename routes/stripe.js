const express = require("express");
const router = express.Router();
const config = require("config");
const stripe = require("stripe")(config.get("stripe.STRIPE_SECRET_KEY"));

const { resolve } = require("path");

/* GET stripe home page. */
router.get("/", (req, res) => {
  const path = resolve("./public/stripe.html");
  res.sendFile(path);
});

/**
 * @swagger
 * tags:
 *   name: STRIPE
 *   description: STRIPE management
 */

/**
 * @swagger
 *
 * /stripe/public-key:
 *   get:
 *     description: Get public key
 *     tags: [STRIPE]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.get("/public-key", (req, res) => {
  res.send({ publicKey: config.get("stripe.STRIPE_PUBLISHABLE_KEY") });
});

/**
 * @swagger
 *
 * /stripe/create-setup-intent:
 *   post:
 *     description: create setup intent
 *     tags: [STRIPE]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.post("/create-setup-intent", async (req, res) => {
  // Create or use an existing Customer to associate with the SetupIntent.
  // The PaymentMethod will be stored to this Customer for later use.
  const customer = await stripe.customers.create();

  res.send(
    await stripe.setupIntents.create({
      customer: customer.id,
    })
  );
});

// Webhook handler for asynchronous events.
router.post("/webhook", async (req, res) => {
  let data;
  let eventType;

  // Check if webhook signing is configured.
  if (config.get("STRIPE_WEBHOOK_SECRET")) {
    // Retrieve the event by verifying the signature using the raw body and secret.
    let event;
    let signature = req.headers["stripe-signature"];

    try {
      event = await stripe.webhooks.constructEvent(
        req.rawBody,
        signature,
        config.get("STRIPE_WEBHOOK_SECRET")
      );
    } catch (err) {
      console.log(`⚠️  Webhook signature verification failed.`);
      return res.sendStatus(400);
    }
    // Extract the object from the event.
    data = event.data;
    eventType = event.type;
  } else {
    // Webhook signing is recommended, but if the secret is not configured in `config.js`,
    // retrieve the event data directly from the request body.
    data = req.body.data;
    eventType = req.body.type;
  }

  if (eventType === "setup_intent.created") {
    console.log(`🔔  A new SetupIntent is created. ${data.object.id}`);
  }

  if (eventType === "setup_intent.setup_failed") {
    console.log(`🔔  A SetupIntent has failed to set up a PaymentMethod.`);
  }

  if (eventType === "setup_intent.succeeded") {
    console.log(
      `🔔  A SetupIntent has successfully set up a PaymentMethod for future use.`
    );
  }

  if (eventType === "payment_method.attached") {
    console.log(
      `🔔  A PaymentMethod ${data.object.id} has successfully been saved to a Customer ${data.object.customer}.`
    );

    // At this point, associate the ID of the Customer object with your
    // own internal representation of a customer, if you have one.

    // Optional: update the Customer billing information with billing details from the PaymentMethod
    const customer = await stripe.customers.update(
      data.object.customer,
      { email: data.object.billing_details.email },
      () => {
        console.log(`🔔  Customer successfully updated.`);
      }
    );
  }

  res.sendStatus(200);
});

/**
 * @swagger
 *
 * /stripe/customers:
 *   get:
 *     description: /charge card off session
 *     tags: [STRIPE]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.get("/customers", async (req, res) => {
  // List the customer's payment methods to find one to charge
  const customers = await stripe.customers.list();
  res.send(customers);
});

/**
 * @swagger
 *
 * /stripe/payment-methods/{id}:
 *   get:
 *     description: /charge card off session
 *     tags: [STRIPE]
 *     parameters:
 *       - name: id
 *         in: path
 *         type: string
 *         required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.get("/payment-methods/:id", async (req, res) => {
  // List the customer's payment methods to find one to charge
  var id = req.params.id;
  console.log("CUSTOMER: " + id);
  const paymentMethods = await stripe.paymentMethods.list({
    customer: id,
    type: "card",
  });
  res.send(paymentMethods);
});

/**
 * @swagger
 *
 * /stripe/charge-card-off-session/{id}:
 *   get:
 *     description: /charge card off session
 *     tags: [STRIPE]
 *     parameters:
 *       - name: id
 *         in: path
 *         type: string
 *         required: true
 *       - name: customer
 *         in: query
 *         type: string
 *         required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.get("/charge-card-off-session/:id", async (req, res) => {
  const id = req.params.id;
  const customer = req.query.customer;
  console.log(id + ":" + customer);
  try {
    // Create and confirm a PaymentIntent with the order amount, currency,
    // Customer and PaymentMethod ID
    paymentIntent = await stripe.paymentIntents.create({
      amount: 180,
      currency: "usd",
      customer: customer,
      payment_method: id,
      off_session: true,
      confirm: true,
    });

    res.send({
      succeeded: true,
      publicKey: config.get("stripe.STRIPE_PUBLISHABLE_KEY"),
      clientSecret: paymentIntent.client_secret,
    });
  } catch (err) {
    if (err.code === "authentication_required") {
      // Bring the customer back on-session to authenticate the purchase
      // You can do this by sending an email or app notification to let them know
      // the off-session purchase failed
      // Use the PM ID and client_secret to authenticate the purchase
      // without asking your customers to re-enter their details
      res.send({
        error: "authentication_required",
        paymentMethod: err.raw.payment_method.id,
        clientSecret: err.raw.payment_intent.client_secret,
        publicKey: config.get("stripe.STRIPE_PUBLISHABLE_KEY"),
        card: {
          brand: err.raw.payment_method.card.brand,
          last4: err.raw.payment_method.card.last4,
        },
      });
    } else if (err.code) {
      // The card was declined for other reasons (e.g. insufficient funds)
      // Bring the customer back on-session to ask them for a new payment method
      res.send({
        error: err.code,
        clientSecret: err.raw.payment_intent.client_secret,
        publicKey: config.get("stripe.STRIPE_PUBLISHABLE_KEY"),
      });
    } else {
      console.log("Unknown error occurred", err);
    }
  }
});

module.exports = router;
