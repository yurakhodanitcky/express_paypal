const express = require("express");
const router = express.Router();
const braintree = require("braintree");
const config = require("config");

const gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  // Use your own credentials from the sandbox Control Panel here
  merchantId: config.get("braintree.merchantId"),
  publicKey: config.get("braintree.publicKey"),
  privateKey: config.get("braintree.privateKey"),
});

/**
 * @swagger
 * tags:
 *   name: BRAINTREE
 *   description: BRAINTREE management
 */

/**
 * @swagger
 *
 * /braintree/customer:
 *   post:
 *     description: Create customer
 *     tags: [BRAINTREE]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.post("/customer/", function (req, res, next) {
  // Use the payment method nonce here
  const nonceFromTheClient = req.body.paymentMethodNonce;
  // Create a new customer
  gateway.customer.create({
      firstName: "Charity",
      lastName: "Smith",
      paymentMethodNonce: nonceFromTheClient,
    },
    function (error, result) {
      if (result) {
        res.send(result);
      } else {
        res.status(500).send(error);
      }
    }
  );
});


/**
 * @swagger
 *
 * /braintree/customers/{id}:
 *   get:
 *     parameters:
 *       - in: path
 *         name: id
 *         type: string
 *         required: true
 *     description: Get customers
 *     tags: [BRAINTREE]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.get("/customers/:id", function (req, res, next) {
  const id = req.params.id;
  gateway.customer.find(id, function (err, customer) {
    if (customer) {
      res.send(customer);
    } else {
      res.status(500).send(err);
    }
  });
});

/**
 * @swagger
 *
 * /braintree/customers/pay/{token}:
 *   get:
 *     parameters:
 *       - in: path
 *         name: token
 *         type: string
 *         required: true
 *     description: Get customers
 *     tags: [BRAINTREE]
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: OK
 *       500:
 *         description: Unexpected error
 */
router.get("/customers/pay/:token", function (req, res, next) {
  const token = req.params.token;
  gateway.transaction.sale({
    amount: "10.00",
    paymentMethodToken: token,
    merchantAccountId: "care",
    options: {
      submitForSettlement: true
    }
  }, function (err, result) {
    if (result.success) {
      res.send(result);
    } else {
      res.status(500).send(result);
    }
  });
});

module.exports = router;